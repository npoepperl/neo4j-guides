= DATEV Browserguides for Neo4J
:Author: DATEV eG
:Email: nils.poepperl@datev.de
:Tags: GraphTour Berlin 2018
:neo4j-version: 3.3.2
:stylesheet: clean.css
:toc:

This is a collection of Browserguides made at DATEV eG to be used in the Neo4J Browser. These guides show how Neo4J is used by DATEV products.

To run any of these guides, please be sure to have the remote host configured in your neo4j.conf:

[source]
----
browser.remote_content_hostname_whitelist=https://npoepperl.gitlab.io
----

For more informations please read link:https://neo4j.com/docs/operations-manual/current/reference/configuration-settings/#config_browser.remote_content_hostname_whitelist[the Neo4J manual].

== Identity and Access Management

The browserguides of the IAM use a stylesheet to visualise the queryresults as graph. If you want to use the same styles in your browser simply run the following command in the Neo4J Browser:

[source]
----
:style https://npoepperl.gitlab.io/neo4j-guides/iam/styles.grass
----

To reset to defaults run:

[source]
----
:style reset
----

=== Simple model without administrative privileges

This is the first version of our graph that doesn't contain special privileged users to be allowed to do the administration of principals within the entities. This guide was shown on the Neo4J GraphTour in Berlin.

[source]
----
:play https://npoepperl.gitlab.io/neo4j-guides/iam/guide-without-super-node.html
----

=== Model with administrative privileges

The second version of the graph now contains principals with administrative privileges. Based on these special nodes the backend used to change data in our graph can efficiently check if the authenticated user is allowed to change the access rights within the entity.

[source]
----
:play https://npoepperl.gitlab.io/neo4j-guides/iam/guide-with-super-node.html
----
