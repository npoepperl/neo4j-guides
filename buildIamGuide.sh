# builds and runs the guide locally
./run.sh ./datev-guides/iam/guide-without-super-node.adoc ./datev-guides/iam/guide-without-super-node.html
./run.sh ./datev-guides/iam/guide-with-super-node.adoc ./datev-guides/iam/guide-with-super-node.html

echo starting http server - press 'ctrl + c' to quit
./http-server.py